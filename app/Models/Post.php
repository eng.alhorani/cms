<?php

namespace App\Models;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;
class Post extends Model
{
    use HasFactory,SoftDeletes;
    protected $fillable = [
        'id',
        'title',
        'description',
        'slug',
        'cover_image',
        'status',
        'user_id',
    ];
protected $hidden=[
    'created_at',
    'updated_at'
];
    public function user()
    {
        return $this->belongsTo(User::class);
    }


}
