<?php

namespace App\Http\Controllers\Api\Post;

use App\Http\Controllers\ApiController;
use App\Http\Controllers\Controller;
use App\Models\Post;
use Illuminate\Http\Request;
use App\Http\Requests\PostRequest;

use App\Repositories\PostRepository;
use Illuminate\Http\JsonResponse;

class PostController extends  ApiController
{
    private $postRepository;

    public function __construct(PostRepository $postRepository){
        $this->postRepository = $postRepository;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request): JsonResponse
    {
//        dd($request->all());
        $limit = $request->get('limit') ? : 10 ;
        if($limit > 30 ) $limit =30 ;
        $request->request->add(['status' => 1]);

        $posts = $this->postRepository
            ->getPosts($request)
            ->paginate($limit);
        $postsPage= $posts->all();
        return $this->respondSuccess($postsPage, $this->createApiPaginator($posts));

    }




    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(PostRequest $request): JsonResponse
    {
        $user = $this->getUser($request);
        if (!$user)
            return $this->respondError(__('api.user_not_found'));
        $request->request->add(['user_id' => $user->id]);
        $this->postRepository->add($request);

        return $this->respondSuccess();
    }



    public function post($id, Request $request): JsonResponse
    {

        $post = Post::query()
            ->where('id', $id)
            ->first();

        if (!$post)
            return $this->respondError(__('api.post_not_found'));


        return $this->respondSuccess($post);
    }





    public function myPosts(Request $request): JsonResponse
    {
        $limit = $request->get('limit') ? : 10 ;
        if($limit > 30 ) $limit =30 ;

        $user = $this->getUser($request);
        if (!$user)
            return $this->respondSuccess(__('api.user_not_found'));

        $posts = Post::query()->whereUserId($user->id)->paginate($limit);


        return $this->respondSuccess($posts->all(), $this->createApiPaginator($posts));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($id, PostRequest $request): JsonResponse
    {
        $post = Post::query()->find($id);
        if (!$post)
            return $this->respondError(__('api.item_not_found'));

        $user = $this->getUser($request);
        if (!$user)
            return $this->respondError(__('api.user_not_found'));

        if ($user->id != $post->user->id)
            return $this->respondError(__('api.unauthorized'));

        $this->postRepository->update($request, $post);

        return $this->respondSuccess();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request): JsonResponse
    {
        $post = Post::query()->find($id);
        if (!$post)
            return $this->respondError(__('api.item_not_found'));

        $user = $this->getUser($request);
        if (!$user)
            return $this->respondError(__('api.user_not_found'));

        if ($user->id != $post->user->id)
            return $this->respondError(__('api.unauthorized'));

        $this->postRepository->delete($post);

        return $this->respondSuccess();
    }


}
