<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Http\Requests\PostRequest;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Repositories\PostRepository;
class PostController extends Controller
{
    private  $postRepository;
    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }
    public function index()
    {
        $posts = Post::orderBy('created_at','desc')->paginate(9);

        return view('home', compact('posts'));
    }
    public function showPost(Post  $post)
    {
        return view('showPost', compact('post'));
    }
//dashboard
    public function dashboard(Request $request)
    {
        $user = User::query()->findOrFail(auth()->id());

        $posts = Post::query()->whereUserId($user->id)->paginate(10);
        return view('dashboard',compact('posts'));
    }
//delete
    public function destroy($id, Request $request)
    {
        $post = Post::query()->find($id);

        $this->postRepository->delete($post);

        return redirect(route('dashboard'));
    }
// store
    public function store(PostRequest $request)
    {
        $this->postRepository->add($request);

        return redirect(route('dashboard'));
    }
//edit
    public function edit(Post $post)
    {
     return   view('create',compact('post'));
    }
    public function update(PostRequest $request,$id)
    {
        $post = Post::query()->find($id);
        $this->postRepository->update($request, $post);

        return   redirect(route('dashboard'));
    }

}
