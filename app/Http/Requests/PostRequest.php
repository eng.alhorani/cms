<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        switch($this->method()) {
            case 'GET':
            case 'PUT':
            $post = $this->route()->post;
            return [
                'title' => 'required|max:100',
                'description' => 'required',
            ];
            case 'PATCH':
            case 'DELETE':
                return [];
            case 'POST':
                return [
                    'title' => 'required|max:100',
                    'description' => 'required',
                ];
            default:break;
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages(): array
    {
        return [
            'title.required' => 'Title  is required!',
            'description.required'  => 'Post description is required',
        ];
    }
}
