<?php

namespace App\Repositories;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use Illuminate\Support\Str;
use function auth;
//use function convertToSeparatedTokens;

class PostRepository {

        public function add(Request $request)
        {
            $post = Post::create([
                'title' => $request->title,
                'description' => $request->description,
                "slug" => Str::slug($request->title, "-"),
                'user_id' => $request->user_id,
            ]);

            $post->cover_image = uploadFile('cover_image', 'posts');

            $post->save();

        }

    public function getPosts(Request $request)
    {
        $posts = Post::query();

        if ($search = $request->get('search')) {
            $tokens = convertToSeparatedTokens($search);
            $posts->whereRaw("MATCH(title, description) AGAINST(? IN BOOLEAN MODE)", $tokens);
        }
        return $posts;
    }


    public function update(Request $request, Post $post)
    {
        $post->update($request->all());
        if ($request->hasFile('cover_image')) {
            // if there is an old background_image delete it
            if ($post->cover_image != null) {
                $post->cover_image = Storage::disk('public')->delete($post->cover_image);
            }
            $post->cover_image = uploadFile('cover_image', 'post');
        }
    $post->save();
        return $post;
    }


    public function delete(Post $post)
    {
//        if ($post->cover_image != null)
//            $post->cover_image = Storage::disk('public')->delete($post->cover_image);

        $post->delete();
    }

}
