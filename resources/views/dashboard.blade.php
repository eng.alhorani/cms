
    <!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <a href="{{route('index')}}"> <title>Cms   </title> </a>
    @include('includes._header')
</head>
<body class="antialiased">
@include('includes._nav')

<div class="container">
    <a href="{{route('create')}}" type="button" class="btn btn-primary my-2">Add</a>
    <table class="table">
        <thead class="thead-dark">
        <tr>
            <th scope="col">Id</th>
            <th scope="col">image</th>
            <th scope="col">Title</th>
            <th scope="col">description</th>
            <th scope="col">Actions</th>
        </tr>
        </thead>
        <tbody>
       @foreach($posts as $post)
        <tr>
            <th scope="row">{{$post->id}}</th>
            <td>
            <img src="{{storageImage($post->cover_image)}}" class="" style="width:75px ; height: 75px">
            </td>
            <td>{{$post->title}}</td>
            <td>{{$post->description}}</td>
            <td>
                <div class="py-2">
                    <form class="float-right ml-2"
                          action="{{route('destroy', $post->id)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger btn-sm">
                            {{--                        {{ $post->trashed() ? 'Delete' : 'Trash' }}--}}
                            Delete
                        </button>
                    </form>
                    <a class="mx-2 btn btn-primary float-right btn-sm" href="{{route('edit',$post)}}">
                        Edit
                    </a>

                </div>


            </td>
        </tr>
       @endforeach


        </tbody>
    </table>


    {{ $posts->links() }}

</div>

@include('includes._scripts')
</body>
</html>
