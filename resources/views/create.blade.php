<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>create post</title>
    @include('includes._header')
</head>
<body class="antialiased">
@include('includes._nav')

<div class="container">


    <div class="card card-default">
        <div class="card-header">
         {{isset($post) ? "add new post":"update post"}}
        </div>
        <div class="card-body">
            <form action="{{isset($post) ? route('update',$post->id) : route('store') }}" method="POST" id="formId" enctype="multipart/form-data">
                @csrf
                @if (isset($post))
                    @method('PUT')
                @endif
                <div class="form-group">
                    <label for="post title">Title:</label>
                    <input type="text" class="form-control" name="title" placeholder="Add a new post" value="{{ isset($post) ? $post->title : "" }}">
                </div>
                <div class="form-group">
                    <label for="post description">Description:</label>
                    <textarea class="form-control" rows="2" name="description" placeholder="Add a description">{{ isset($post) ? $post->description : "" }}</textarea>
                </div>

                <div class="form-group">
                    <label for="post image">Image:</label>
                    <input type="file" class="form-control" name="cover_image">
                </div>

                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">
                <div class="form-group">
                    <button type="submit" class="btn btn-success">
                      {{ isset($post) ? "Update" : "Add" }}
                    </button>
                </div>
            </form>
        </div>
    </div>



</div>
@include('includes._scripts')
<script>

</script>

</body>
</html>
