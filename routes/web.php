<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Front\PostController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});
Route::get('/', [PostController::class, 'index'])->name('index');
Route::get('/post/{post}', [PostController::class, 'showPost'])->name('showPost');

Route::group(['middleware'=>'auth'],function (){
    Route::get('/dashboard', [PostController::class, 'dashboard'])->name('dashboard');
    Route::delete('/destroy/{id}', [PostController::class, 'destroy'])->name('destroy');
    Route::post('/store', [PostController::class, 'store'])->name('store');
    Route::get('/create',function () {
        return view('create');
    })->name('create');

    Route::get('/edit/{post}',[PostController::class,'edit'])->name('edit');
    Route::put('/update/{id}',[PostController::class,'update'])->name('update');

});
require __DIR__.'/auth.php';
