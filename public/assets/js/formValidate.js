
console.log("hi");

jQuery(document).ready(function() {
    let $registerForm =$('#formId');
    if ($registerForm.length) {
        $registerForm.validate({
            rules: {
                title: {
                    required: true
                },
                description: {
                    required: true,
                },

            },
            messages: {
                'title': {
                    required: 'please enter heading'
                },
                'description': {
                    required: 'please enter main heading'
                },

            }
        });
    }
});
